module ATNF
    using PyCall
    import JSON

    py"""
    import os
    import json

    import numpy as np

    class Pulsar:

        def __init__(self):
            self.PSRJ = None
            self.PSRB = None
            self.P0 = None
            self.P1 = None
            self.F0 = None
            self.F1 = None
            self.S1400 = None
            self.DM = None
            self.SURVEY = None
            self.TYPE = None
            self.W50 = None
            self.W10 = None


    class Pulsars:

        def __init__(self):
            self.records = []
            self.indexes = None
            self.values = []

        def get_survey(self, name='pksmb'):
            indexes = []
            for i, r in enumerate(self.records):
                try:
                    if name in r['SURVEY'][0]:
                        indexes.append(i)
                except:
                    pass
            self.indexes = indexes

        def get_values(self, val, index=0, conv=None):
            #Gets data from pulsars.json
            v_ = []
            for i in self.indexes:
                try:
                    v = self.records[i][val][index]
                except TypeError:
                    v = None
                if v is not None:
                    if conv is not None:
                        v_.append(conv(v))
                    else:
                        v_.append(v)
                else:
                    #print self.records[i]['PSRJ'][index]
                    pass
            self.values = np.array(v_)
            return np.array(v_)

        def get_values2D(self, val1, val2, index=0, conv=None):
            #Gets data from pulsars.json, two values at once
            v_ = [[], []]
            for i in self.indexes:
                try:
                    v1 = self.records[i][val1][index]
                    v2 = self.records[i][val2][index]
                except TypeError:
                    v1 = None
                    v2 = None
                if v1 is not None and v2 is not None:
                    if conv is not None:
                        v_[0].append(conv(v1))
                        v_[1].append(conv(v2))
                    else:
                        v_[0].append([v1])
                        v_[1].append([v2])
                else:
                    pass
            self.values = np.array(v_)
            return np.array(v_)


        def save_data(self, out_file):
            s = json.dumps(self, cls=PulsarsEncoder, indent=4, sort_keys=True)
            f = open(out_file, 'w')
            f.write(s)
            f.close()

        @staticmethod
        def load(file_name):
            f = open(file_name)
            pu = json.load(f, cls=PulsarsDecoder)
            f.close()
            return pu

        @staticmethod
        def convert_db(in_file, out_file):
            #Converts ATNF db file to json
            psrs = Pulsars()
            f = open(in_file)
            lines = f.readlines()
            p = Pulsar()
            fields = dir(p)
            # skip first 5 lines
            for i in range(5, len(lines)):
                if lines[i].startswith('@-'):
                    psrs.records.append(p)
                    p = Pulsar()
                else:
                    res = lines[i].split()
                    if res[0] in fields:
                        setattr(p, res[0], res[1:])
            f.close()
            # complete PO, F1 records
            for p in psrs.records:
                if p.P0 is None and p.F0 is not None:
                    p.P0 = [1. / float(p.F0[0])]
                elif p.P0 is not None and p.F0 is None:
                    p.F0 = [1. / float(p.P0[0])]
                if p.P1 is None and p.F1 is not None:
                    p.P1 = [- float(p.F0[0]) ** -2. * float(p.F1[0])]
                elif p.P1 is not None and p.F1 is None:
                    p.F1 = [- float(p.P0[0]) ** -2 * float(p.P1[0])]
            psrs.save_data(out_file)


    class PulsarsDecoder(json.JSONDecoder):

        def decode(self, json_string):
            #:param json_string: is basically string that you give to json.loads method
            obj = json.loads(json_string)
            pu = Pulsars()
            for key, value in obj.items():
                setattr(pu, key, value)
            return pu


    class PulsarsEncoder(json.JSONEncoder):

        def default(self, o):
            if isinstance(o, np.ndarray):
                if o.ndim == 1:
                    return o.tolist()
                else:
                    return [self.default(o[i]) for i in range(o.shape[0])]
            elif isinstance(o, (complex, np.complex)):
                return [o.real, o.imag]
            else:
                return o.__dict__
    """


    """
        Converts psrcar.db to JSON file. For new fields change Pulsar python Class and run convert
    """
    function convert_db(;infile="input/psrcat.db")
        py"Pulsars.convert_db"(infile, "input/pulsars.json")
    end

    function load(;filename="input/pulsars.json")
        f = open(filename)
        json = JSON.parse(f)
        close(f)
        return json["records"]
    end


end
