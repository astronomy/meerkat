module Functions

    function edot(period, pdot)
        return 3.95e31 * pdot / 1e-15 * period ^ (-3)
    end

    function p3_rahul(edot)
        return (edot / 2.3e32) ^ -0.6
    end

    function p3_andrzej(edot)
        return (edot / 1.5e32) ^ -0.76
    end

    function p3_andrzej2(edot)
        return (edot / 2.8e32) ^ 0.19
    end


end # module
