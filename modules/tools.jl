module Tools
    using FFTW
    using Peaks
    using LsqFit
    using Statistics

    """
    Creates symbolic links to data
    """
    function dolinks(;datadir="/fred/oz005/search/J0630-2834/2019-04-16-19:17:17/1444.5/")
        files = readdir(datadir)
        for f in files
            symlink("$datadir/$f", "$f")
        end
    end


    """
    Creates symbolic links to data in outdir
    """
    function dolinks(outdir; datadir="/fred/oz005/search/J0630-2834/2019-04-16-19:17:17/1444.5/")
        files = readdir(datadir)
        for f in files
            symlink("$datadir/$f", "$outdir/$f")
        end
    end


    """
    Creates files with Ephemerides
    """
    function create_ephs(;outdir="output/ephemerides/", full_run=false)
        # create Ephemeries file for all pulsars
        if full_run == true
            run(pipeline(`psrcat -e`, "$outdir/ephs.txt"))
        end
        f = open("$outdir/ephs.txt")
        for line in readlines(f)
            global f2
            if startswith(line, "PSRJ")
                jname = split(line)[2]
                #println(jname)
                f2 = open("$outdir/$jname.par", "w")
                #println("start")
                write(f2, "$line\n")
            elseif startswith(line, "@-")
                close(f2)
            else
                write(f2, "$line\n")
            end
        end
        close(f)
        close(f2)
    end


    """
    Download or update meertime ephemerides
    """
    function download_meertime_ephs(;outdir="output/ephemerides/", update=true)
        olddir = pwd()
        if update == false
            cd(outdir)
            run(pipeline(`git clone https://_Andrzej_@bitbucket.org/meertime/meertime_ephemerides.git`))
        else
            cd("$outdir/meertime_ephemerides/")
            run(pipeline(`git pull`))
        end
        cd(olddir)
    end


    function average_profile(data)
        pulses, bins = size(data)
        ave = zeros(bins)
        for i in 1:pulses
            for j in 1:bins
                ave[j] += data[i,j]
            end
        end
        ma = maximum(ave)
        ave = ave / ma
        return ave
    end


    function intensity_pulses(data)
        pulse_num, bins = size(data)
        intensity = zeros(pulse_num)
        for i in 1:pulse_num
            intensity[i] = sum(data[i])
        end
        pulses = collect(1:pulse_num)
        mi = minimum(intensity)
        if mi < 0
            intensity .+= -mi
        end
        intensity /= maximum(intensity)
        return (intensity, pulses)
    end

    function fftfreq(n, d=1.0)
        # one side (positive) frequancy range
        if iseven(n)
            rf = range(0, convert(Int, n/2 - 1), step=1)
        else
            rf = range(0, convert(Int, (n-1)/2 - 1), step=1)  # -1 added to match size
        end
        f = collect(rf) ./ (d * n)
        return f
    end


    function lrfs(data)
        da = transpose(data)
        bins, pulse_num = size(da)
        half = floor(Int, pulse_num / 2) # one side frequency range?
        lrfs = fft(da, 2)[:, 1:half] # second dim! important!
        lrfs = transpose(lrfs)
        freq = fftfreq(pulse_num)
        intensity = zeros(half)
        ab = abs.(lrfs)
        for i in 1:half
            intensity[i] = sum(ab[i,:]) # this is important!
        end
        pk = peaks(intensity)
        return lrfs, intensity, freq, pk
    end


    function peaks(intensity)
        #max = maxima(intensity, 30)
        ma, pa = peakprom(intensity, Maxima(), floor(Int, length(intensity)/20))
        val , ind = findmax(pa)
        return ma[ind]
    end

    #=
    @. # NOPE!
    function gauss(x, p)
        return p[1] * exp(-0.5*((x-p[2])/p[3])^2) + p[4]
    end
    =#

    @. gauss(x, p) = p[1] * exp(-0.5*((x-p[2])/p[3])^2) + p[4]

    function fit_gaussian(xdata, ydata; a=nothing, μ=nothing, σ=nothing, baselevel=nothing)
        if a == nothing
            a = maximum(ydata)
        end
        if μ == nothing
            ind = trunc(Int, length(xdata) / 2)
            μ = xdata[ind]
        end
        if σ == nothing
            σ = xdata[20] - xdata[1]
        end
        if baselevel == nothing
            baselevel = mean(ydata)
        end
        # how to use gauss here? done!
        #@. model(x, p) = p[1] * exp(-0.5*((x-p[2])/p[3])^2) + p[4]

        p0 = [a, μ, σ, baselevel]  # look here
        fit = curve_fit(gauss, xdata, ydata, p0)
        p = coef(fit)
        err = stderror(fit)
        return p, err
    end


    @. twogauss(x, p) = p[1] * exp(-0.5*((x-p[2])/p[3])^2)  + p[4] * exp(-0.5*((x-p[5])/p[6])^2) + p[7]

    function fit_twogaussians(xdata, ydata, a1, a2, μ1, μ2, σ1, σ2; baselevel=nothing)
        if baselevel == nothing
            baselevel = median(ydata)
        end

        p0 = [a1, μ1, σ1, a2, μ2, σ2, baselevel]  # look here
        fit = curve_fit(twogauss, xdata, ydata, p0)
        p = coef(fit)
        err = stderror(fit)
        return p, err
    end


    @. threegauss(x, p) = p[1] * exp(-0.5*((x-p[2])/p[3])^2)  + p[4] * exp(-0.5*((x-p[5])/p[6])^2) + p[7] * exp(-0.5*((x-p[8])/p[9])^2) + p[10]

    function fit_threegaussians(xdata, ydata, a1, a2, a3, μ1, μ2, μ3, σ1, σ2, σ3; baselevel=nothing)
        if baselevel == nothing
            baselevel = median(ydata)
        end
        p0 = [a1, μ1, σ1, a2, μ2, σ2, a3, μ3, σ3, baselevel]  # look here
        fit = curve_fit(threegauss, xdata, ydata, p0)
        p = coef(fit)
        err = stderror(fit)
        return p, err
    end


    function find_peaks(data)
        sz = size(data)
        peaks = []
        for i in 1:sz[1]
            signal = data[i, :]
            xdata = collect(0:length(signal)-1) # from 0 python plotting
            peak = Tools.peaks(signal)
            ma, pa = peakprom(signal, Maxima(), 3)
            inds = sortperm(pa, rev=true)
            if length(inds) > 2 && (pa[inds[3]] > 0.5 * pa[inds[2]]) # lazy, but works
                #println("three $i")
                pa, errs = Tools.fit_threegaussians(xdata, signal, pa[inds[1]], pa[inds[2]], pa[inds[3]], xdata[ma[inds[1]]], xdata[ma[inds[2]]], xdata[ma[inds[3]]], 3, 3, 3)
                push!(peaks, [i-1, pa[2], pa[5], pa[8]])  # python plotting!
            elseif length(inds) > 1
                #println("two $i")
                pa, errs = Tools.fit_twogaussians(xdata, signal, pa[inds[1]], pa[inds[2]], xdata[ma[inds[1]]], xdata[ma[inds[2]]], 3, 3)
                push!(peaks, [i-1, pa[2], pa[5]])  # python plotting!
            end
        end
        return peaks
    end



end # module
