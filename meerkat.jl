#! /usr/bin/env julia
include("modules/tools.jl")
using JLD
module MeerKAT
    using Dates
    using JLD
    using Printf
    using Glob
    include("modules/atnf.jl")
    include("modules/tools.jl")
    include("modules/functions.jl")
    include("modules/data.jl")
    include("modules/plot.jl")

    mutable struct Pulsar
        jname
        p
        pdot
        edot
        w10
        w50
        observations
    end
    Pulsar() = Pulsar(nothing, nothing, nothing, 0., nothing, nothing, [])

    mutable struct Observation
        date
        duration  # in seconds
        freq
        rawdata  # raw data
        prdata  # processed data
    end

    """
    Creates list of pulsar observations
    """
    function create_list(;dir="/fred/oz005/search", get_par=false)
        pulsars = []
        frmt = DateFormat("y-m-d-H:M:S")
        endssf(x) = endswith(x, ".sf")
        psrs = filter(isdir, ["$dir/$d" for d in readdir(dir)])
        for psr in psrs
            sessions = filter(isdir, ["$psr/$s" for s in readdir(psr)])
            for se in sessions
                freqs = filter(isdir, ["$se/$fr" for fr in readdir(se)])
                for f in freqs
                    obs = filter(endssf, readdir(f))
                    # gert obs duration
                    t_ = []
                    for o in obs
                        try
                            dt = DateTime(o[1:end-3], frmt)
                            push!(t_, datetime2unix(dt))
                        catch
                            println("Warning! parse error in $psr $(o[1:end-3])")
                        end
                    end
                    sort!(t_)
                    #println(f)
                    duration = 0.
                    try
                        duration = t_[end]-t_[1]
                    catch
                    end
                    if duration != 0
                        # add obs to the list
                        jname, ses, freq = split(replace(f, dir => ""), "/")[2:end]  # this is so bad
                        pulsar = nothing
                        # find pulsar
                        for p in pulsars
                            if p.jname == jname
                                pulsar = p
                                break
                            end
                        end
                        if pulsar == nothing
                            pulsar = Pulsar(jname, nothing, nothing, 0., nothing, nothing, [])
                            push!(pulsars, pulsar)
                        end
                        # add observation
                        freq = parse(Float64, freq)
                        date = DateTime(ses, frmt)
                        prdata = "/fred/oz005/users/aszary/search/$(pulsar.jname)/$date/$freq/"
                        push!(pulsar.observations, Observation(date, duration, freq, f, prdata))
                    end
                end
            end
        end
        if get_par == true
            #ATNF.convert_db(;infile="input/psrcat_custom.db")
            records = ATNF.load()
            for pulsar in pulsars
                get_params!(pulsar, records)
            end
        end
        return pulsars
    end

    """
    Saves pulsar observation list
    """
    function save_list(pulsars; filename="output/psrs.txt")
        #ATNF.convert_db(;infile="input/psrcat_custom.db")
        records = ATNF.load()
        for pulsar in pulsars
            get_params!(pulsar, records)
        end
        sort!(pulsars, by=p->p.edot, rev=false)
        #
        f = open(filename, "w")
        write(f, "#NAME\tEdot\tPeriod\tPHASE10\tPHASE50\tFREQ\tPULSES\n")
        for pulsar in pulsars
            for (i, obs) in enumerate(pulsar.observations)
                if pulsar.w10 != nothing
                    ph10 = @sprintf("%.2f" ,pulsar.w10 * 1e-3 / pulsar.p)
                else
                    ph10 = "---"
                end
                if pulsar.w50 != nothing
                    ph50 = @sprintf("%.2f" ,pulsar.w50 * 1e-3 / pulsar.p)
                else
                    ph50 = "---"
                end
                pulses = "---"
                try
                    pulses = trunc(Int, obs.duration / pulsar.p)
                catch
                end
                edot = @sprintf("%.1e", pulsar.edot)
                if pulsar.p != nothing
                    period = @sprintf("%.3f", pulsar.p)
                else
                    period = "---"
                end
                if i == 1
                    write(f, "$(pulsar.jname)\t$edot\t$period\t$ph10\t$ph50\t$(obs.freq)\t$pulses\n")
                else
                    write(f, "-----------\t$edot\t$period\t$ph10\t$ph50\t$(obs.freq)\t$pulses\n")
                end
            end
        end
        close(f)
    end

    """
    Gets pulsar data from ATNF
    """
    function get_params!(pulsar, records, verbose=0)
        for psr in records
            if psr["PSRJ"][1] == pulsar.jname
                try
                    pulsar.p = psr["P0"][1]
                    if typeof(pulsar.p) == String
                        pulsar.p = parse(Float64, pulsar.p)
                    end
                catch
                    if verbose == 1 println("Warning! No PO for $(pulsar.jname)") end
                end
                try
                    pulsar.pdot = psr["P1"][1]
                    if typeof(pulsar.pdot) == String
                        pulsar.pdot = parse(Float64, pulsar.pdot)
                    end
                catch
                    if verbose == 1 println("Warning! No P1 for $(pulsar.jname)") end
                end
                if pulsar.p != nothing && pulsar.pdot != nothing
                    pulsar.edot = Functions.edot(pulsar.p, pulsar.pdot)
                end
                try
                    pulsar.w10 = psr["W10"][1]
                    if typeof(pulsar.w10) == String
                        pulsar.w10 = parse(Float64, pulsar.w10)
                    end
                catch
                    if verbose == 1 println("Warning! No W10 for $(pulsar.jname)") end
                end
                try
                    pulsar.w50 = psr["W50"][1]
                    if typeof(pulsar.w50) == String
                        pulsar.w50 = parse(Float64, pulsar.w50)
                    end
                catch
                    if verbose == 1 println("Warning! No W50 for $(pulsar.jname)") end
                end
                break
            end
        end
    end

    """
    Generate single pulses using dspsr and cluster runs
    """
    function generate_all(pulsars; run_script=true, pulse_number=128, outdir="/fred/oz005/users/aszary/search/", ephdir="/home/aszary/software/meerkat/output/ephemerides/", verbose=0)
        olddir = pwd()
        frmt = DateFormat("y-m-d-H:M:S")
        println(length(pulsars))
        for pulsar in pulsars
            for obs in pulsar.observations
                if obs.duration != 0.
                    # data path
                    pth = "$outdir/$(pulsar.jname)/$(obs.date)/$(obs.freq)/"
                    mkpath(pth)
                    println(pth)
                    obs.prdata = pth
                    # create data links
                    try
                        Tools.dolinks(pth; datadir=obs.rawdata)
                    catch err
                        if isa(err, Base.IOError)
                            if verbose == 1
                                println("Warning! Files $pth already exists?")
                            end
                        end
                    end
                    # copy ephemeris
                    if isfile("$pth/$(pulsar.jname).par") == false
                        if isfile("$ephdir/meertime_ephemerides/$(pulsar.jname).par")
                            cp("$ephdir/meertime_ephemerides/$(pulsar.jname).par", "$pth/$(pulsar.jname).par")
                        else
                            try
                                cp("$ephdir/$(pulsar.jname).par", "$pth/$(pulsar.jname).par")
                            catch
                                println("Error. No ephemeris files for $(pulsar.jname). Skipping...")
                                continue
                            end
                        end
                    end
                    # create script file
                    try
                        dt = pulsar.p * pulse_number
                    catch
                        println(pulsar)
                        println("Error! No pulsar period data for PSR $(pulsar.jname)...")
                        println("Check / improve / convert_db")
                        return
                        continue
                    end
                    if dt > obs.duration
                        dt = obs.duration
                        process_files = sort(glob("*sf", pth))
                    else
                        # determine number of files (especially important for millisecond pulsars)
                        files = sort(glob("*sf", pth))
                        process_files = []
                        ts = []
                        for fi in files
                            #datetime = DateTime(fi, frmt)
                            datetime = DateTime(split(split(fi, "/")[end], ".")[1], frmt)
                            push!(ts, datetime)
                            #println(datetime)
                        end
                        t = 0
                        for ii in 1:length(ts)-1
                            dtt = ts[ii+1] - ts[ii]
                            t +=  Dates.value(Second(dtt))
                            push!(process_files, files[ii])
                            if t > dt
                                break
                            end
                            #println(typeof(dt))
                        end
                        #println(process_files)
                    end
                    prfiles = ""
                    for pf in process_files
                        prfiles *= " $pf"
                    end
                    #println(prfiles)
                    scr = """#!/bin/bash
#SBATCH -n 1
#SBATCH -t 1:00:00
#SBATCH --mail-user=aszary@gmail.com
#SBATCH --mem=3gb
time dspsr -b 1024 -t 1 -scloffs -U 2048 -A -T $dt -skz -K -s -E $(pulsar.jname).par -O $pulse_number *.sf
paz -r -e rr $pulse_number.ar
pdv -t -F -p $pulse_number.rr > $pulse_number.txt
rm $pulse_number.ar
rm $pulse_number.rr
"""
#psradd -o grand.rr p*.rr # out of memory problem, even with --mem=1gb?
#rm p*.rr
                    f = open("$pth/$(pulsar.jname).sh", "w")
                    write(f, scr)
                    close(f)
                    # run script
                    if run_script == true
                        cd(pth)
                        run(`sbatch $(pulsar.jname).sh`)
                    end
                end
            end
        end
        #dspsr -b 1024 -t 1 -scloffs -U 128 -K -s -E J0630-2834.par *sf
        # save pulsar data
        cd(olddir)
        JLD.save("output/pulsar_data.jld", "pulsars", pulsars)
    end



    """
    Generate single pulses for one pulsar using dspsr and cluster run
    """
    function generate_one(jname, pulsars; run_script=true, pulse_number=232, outdir="/fred/oz005/users/aszary/search/", ephdir="/home/aszary/software/meerkat/output/ephemerides/", verbose=0)
        olddir = pwd()
        frmt = DateFormat("y-m-d-H:M:S")
        println(length(pulsars))
        for pulsar in pulsars
            if pulsar.jname == jname
                for obs in pulsar.observations
                    if obs.duration != 0.
                        # data path
                        pth = "$outdir/$(pulsar.jname)/$(obs.date)/$(obs.freq)/"
                        println(pth)
                        mkpath(pth)
                        number_pulses = obs.duration / pulsar.p
                        println("Number of pulses: $number_pulses")
                        #return
                        obs.prdata = pth
                        # create data links
                        try
                            Tools.dolinks(pth; datadir=obs.rawdata)
                        catch err
                            if isa(err, Base.IOError)
                                if verbose == 1
                                    println("Warning! Files $pth already exists?")
                                end
                            end
                        end
                        # copy ephemeris
                        if isfile("$pth/$(pulsar.jname).par") == false
                            if isfile("$ephdir/meertime_ephemerides/$(pulsar.jname).par")
                                cp("$ephdir/meertime_ephemerides/$(pulsar.jname).par", "$pth/$(pulsar.jname).par")
                            else
                                cp("$ephdir/$(pulsar.jname).par", "$pth/$(pulsar.jname).par")
                            end
                        end
                        # create script file
                        try
                            dt = pulsar.p * pulse_number
                        catch
                            println(pulsar)
                            println("Error! No pulsar period data for PSR $(pulsar.jname)...")
                            println("Check / improve / convert_db")
                            return
                            continue
                        end
                        if dt > obs.duration
                            dt = obs.duration
                            process_files = sort(glob("*sf", pth))
                        else
                            # determine number of files (especially important for millisecond pulsars)
                            files = sort(glob("*sf", pth))
                            process_files = []
                            ts = []
                            for fi in files
                                datetime = DateTime(split(split(fi, "/")[end], ".")[1], frmt)
                                push!(ts, datetime)
                            end
                            t = 0
                            for ii in 1:length(ts)-1
                                dtt = ts[ii+1] - ts[ii]
                                t +=  Dates.value(Second(dtt))
                                push!(process_files, files[ii])
                                if t > dt
                                    break
                                end
                            end
                        end
                        prfiles = ""
                        for pf in process_files
                            prfiles *= " $pf"
                        end
                        scr = """#!/bin/bash
#SBATCH -n 1
#SBATCH -t 1:00:00
#SBATCH --mail-user=aszary@gmail.com
#SBATCH --mem=9gb
time dspsr -b 1024 -t 1 -scloffs -U 2048 -A -T $dt -skz -K -s -E $(pulsar.jname).par -O $pulse_number *.sf
paz -r -e rr $pulse_number.ar
pdv -t -F -p $pulse_number.rr > $pulse_number.txt
rm $pulse_number.ar
#rm $pulse_number.rr
    """
    #time dspsr -b 1024 -t 1 -scloffs -U 2048 -T $dt -K -s -E $(pulsar.jname).par $prfiles
    #psradd -o grand.rr p*.rr # out of memory problem, even with --mem=1gb?
    #rm p*.rr

                        f = open("$pth/$(pulsar.jname).sh", "w")
                        write(f, scr)
                        close(f)
                        # run script
                        if run_script == true
                            cd(pth)
                            run(`sbatch $(pulsar.jname).sh`)
                        end
                    end
                end
                break
            end # end if
        end # end for
        cd(olddir)
    end


    """
    Generate single pulses for one pulsar and one obs. dir using dspsr and cluster run
    """
    function generate_onedir(jname, pulsars, obs_num; run_script=true, pulse_number=232, outdir="/fred/oz005/users/aszary/search/", ephdir="/home/aszary/software/meerkat/output/ephemerides/", verbose=0)
        olddir = pwd()
        frmt = DateFormat("y-m-d-H:M:S")
        println(length(pulsars))
        for pulsar in pulsars
            if pulsar.jname == jname
                obs = pulsar.observations[obs_num]
                # data path
                pth = "$outdir/$(pulsar.jname)/$(obs.date)/$(obs.freq)/"
                println(pth)
                mkpath(pth)
                number_pulses = obs.duration / pulsar.p
                println("Number of pulses: $number_pulses")
                #return
                obs.prdata = pth
                # create data links
                try
                    Tools.dolinks(pth; datadir=obs.rawdata)
                catch err
                    if isa(err, Base.IOError)
                        if verbose == 1
                            println("Warning! Files $pth already exists?")
                        end
                    end
                end
                # copy ephemeris
                if isfile("$pth/$(pulsar.jname).par") == false
                    if isfile("$ephdir/meertime_ephemerides/$(pulsar.jname).par")
                        cp("$ephdir/meertime_ephemerides/$(pulsar.jname).par", "$pth/$(pulsar.jname).par")
                    else
                        cp("$ephdir/$(pulsar.jname).par", "$pth/$(pulsar.jname).par")
                    end
                end
                # create script file
                try
                    dt = pulsar.p * pulse_number
                catch
                    println(pulsar)
                    println("Error! No pulsar period data for PSR $(pulsar.jname)...")
                    println("Check / improve / convert_db")
                    return
                    continue
                end
                if dt > obs.duration
                    dt = obs.duration
                    process_files = sort(glob("*sf", pth))
                else
                    # determine number of files (especially important for millisecond pulsars)
                    files = sort(glob("*sf", pth))
                    process_files = []
                    ts = []
                    for fi in files
                        datetime = DateTime(split(split(fi, "/")[end], ".")[1], frmt)
                        push!(ts, datetime)
                    end
                    t = 0
                    for ii in 1:length(ts)-1
                        dtt = ts[ii+1] - ts[ii]
                        t +=  Dates.value(Second(dtt))
                        push!(process_files, files[ii])
                        if t > dt
                            break
                        end
                    end
                end
                prfiles = ""
                for pf in process_files
                    prfiles *= " $pf"
                end
                scr = """#!/bin/bash
#SBATCH -n 1
#SBATCH -t 1:00:00
#SBATCH --mail-user=aszary@gmail.com
#SBATCH --mem=9gb
time dspsr -b 1024 -t 1 -scloffs -U 2048 -A -T $dt -skz -K -s -E $(pulsar.jname).par -O $pulse_number *.sf
paz -r -e rr $pulse_number.ar
pdv -t -F -p $pulse_number.rr > $pulse_number.txt
rm $pulse_number.ar
#rm $pulse_number.rr
    """
    #time dspsr -b 1024 -t 1 -scloffs -U 2048 -T $dt -K -s -E $(pulsar.jname).par $prfiles
    #psradd -o grand.rr p*.rr # out of memory problem, even with --mem=1gb?
    #rm p*.rr

                f = open("$pth/$(pulsar.jname).sh", "w")
                write(f, scr)
                close(f)
                # run script
                if run_script == true
                    cd(pth)
                    run(`sbatch $(pulsar.jname).sh`)
                end
                break
            end # end if
        end # end for
        cd(olddir)
    end



    """
        Re-run cluster scripts
    """
    function  rerun_all(pulsars; outdir="/fred/oz005/users/aszary/search/", force=false)
        if pulsars == nothing
            pulsars = JLD.load("output/pulsar_data.jld", "pulsars")
        end
        olddir = pwd()
        for pulsar in pulsars
            for obs in pulsar.observations
                println(obs.prdata)
                cd(obs.prdata)
                files = glob("*txt", obs.prdata)
                if force == true
                    run(`sbatch $(pulsar.jname).sh`)
                elseif length(files) == 0
                    try
                        run(`sbatch $(pulsar.jname).sh`)
                    catch
                        println("Error. No script file for $(pulsar.jname). Skipping...")
                    end
                end
            end
        end
        println(length(pulsars))
        cd(olddir)
    end


    """
    Analyse all observations using julia
    """
    function analyse_all(pulsars; pulse_number=128, remove=true)
        olddir = pwd()
        if pulsars == nothing
            pulsars = JLD.load("output/pulsar_data.jld", "pulsars")
        end

        no_data = 0
        data = 0
        skip = false
        for pulsar in pulsars
            for obs in pulsar.observations
                println(obs.prdata)
                filename = "$(obs.prdata)/$pulse_number.txt"
                if isfile(filename)
                    try
                        global da
                        da = Data.load_ascii(filename)
                        data += 1
                    catch
                        no_data += 1
                        println("Warning! Empty file, removing...")
                        rm(filename)
                        skip = true
                    end
                    if skip == false
                        #=
                        # remove old plots
                        cd(obs.prdata)
                        pdfs = glob("*.pdf")
                        println("$pdfs")
                        for p in pdfs
                            rm(p)
                            println("removed $p")
                        end
                        =#
                        Plot.single(da, "$(obs.prdata)"; darkness=0.5, number=nothing, name_mod="$(pulsar.jname)_pnumber_$(pulse_number)")
                        try
                            Plot.lrfs(da, "$(obs.prdata)"; darkness=0.1, start=1, name_mod="$(pulsar.jname)_pnumber_$(pulse_number)", change_fftphase=false)
                        catch
                            println("Warning! LRFS failed...")
                        end
                    else
                        skip = false
                    end
                else
                    no_data += 1
                end
            end
        end
        println(length(pulsars))
        println("Data num: $data, No data: $no_data")
        cd(olddir)
    end


    """
    Analyse all observations using spa.py python package
    OBSOLETE!
    """
    function analyse_old(pulsars)
        olddir = pwd()
        if pulsars == nothing
            pulsars = JLD.load("output/pulsar_data.jld", "pulsars")
        end
        no_data = 0
        data = 0
        for pulsar in pulsars
            for obs in pulsar.observations
                println(obs.prdata)
                files = glob("*rr", obs.prdata)
                if length(files) != 0
                    cd(obs.prdata)
                    try
                        run(`/home/aszary/software/spa/spa.py $(obs.prdata)`)
                    catch
                        println("ERROR: $(obs.prdata)")
                    end
                    data += 1
                else
                    no_data += 1
                end
            end
        end
        println(length(pulsars))
        println("Data num: $data, No data: $no_data")
        cd(olddir)
    end

    """
    Analyse one observation
    """
    function analyse_one(jname, pulsars; pulse_num=128, number=nothing, obs_num=2, outdir="output/", bin_st=300, bin_end=650)
        if pulsars == nothing
            pulsars = JLD.load("output/pulsar_data.jld", "pulsars")
        end
        for pulsar in pulsars
            if pulsar.jname == jname
                filename = "$(pulsar.observations[obs_num].prdata)/$(pulse_num).txt"
                println(filename)
                println("Number of pulses: ", pulsar.observations[obs_num].duration / pulsar.p)
                da = Data.load_ascii(filename)
                Plot.single(da, outdir; darkness=0.5, number=number, name_mod="$(jname)_one")
                Plot.lrfs(da, outdir; darkness=0.1, start=1, number=number, name_mod="$(jname)_one", change_fftphase=false)
                #Plot.single(da, outdir; darkness=0.5, number=60, name_mod="$(jname)_one2", bin_st=bin_st, bin_end=bin_end)
                #Plot.lrfs(da, outdir; darkness=0.1, start=1, name_mod="$(jname)_one2", change_fftphase=false, bin_st=bin_st, bin_end=bin_end)
            end
        end
    end

    """
    Copy PDF files for all pulsars to outdir
    """
    function preview_all(pulsars; outdir="output/preview")
        #olddir = pwd()
        if pulsars == nothing
            pulsars = JLD.load("output/pulsar_data.jld", "pulsars")
        end
        for pulsar in pulsars
            for (i, obs) in enumerate(pulsar.observations)
                files = sort(glob("*pdf", obs.prdata))
                for f in files
                    println(f)
                    outfile = split(f, "/")[end]
                    of = replace(outfile, ".pdf"=>"_$i.pdf")
                    cp(f, "$outdir/$of")
                end
            end
        end
        #cd(olddir)
    end


    function recalculate_drifters(pulsars; filename="input/drifters.txt", run_script=true)
        f = open(filename)
        for line in readlines(f)
            jname = split(line)[1]
            for pulsar in pulsars
                if pulsar.jname == jname
                    println("$(pulsar.jname)")
			        for (i,obs) in enumerate(pulsar.observations)
				        pulse_number = obs.duration / pulsar.p
                        if pulse_number > 512
                            MeerKAT.generate_onedir(jname, pulsars, i; pulse_number=512, run_script=run_script)
                            println("\t$i 512")
                        else
                            MeerKAT.generate_onedir(jname, pulsars, i; pulse_number=trunc(Int, pulse_number)-1, run_script=run_script)
                            println("\t$i $(pulse_number-1)")
                        end
                    end
                    break
                end
            end
        end
        close(f)
    end


    function reanalyse_drifters(pulsars; filename="input/drifters.txt")
        f = open(filename)
        for line in readlines(f)
            jname = split(line)[1]
            for pulsar in pulsars
                if pulsar.jname == jname && jname != "J0630-2834" && jname != "J1502-6128" && jname != "J1612-5805" && jname != "J1622-4950"
                    println("$(pulsar.jname)")
			        for (i,obs) in enumerate(pulsar.observations)
				        pulse_number = obs.duration / pulsar.p
                        if pulse_number > 512
                            analyse_one(jname, pulsars; pulse_num=512, obs_num=i)
                            println("\t$i 512")
                        else
                            analyse_one(jname, pulsars; pulse_num=trunc(Int, pulse_number)-1, obs_num=i)
                            println("\t$i $(pulse_number-1)")
                        end
                    end
                    break
                end
            end
        end
        close(f)
    end


    function new_observations_old(previous)
        current_pulsars = create_list(get_par=true)
        previous_pulsars = JLD.load(previous, "pulsars")
        #println(length(current_pulsars))
        #println(length(previous_pulsars))
        pulsars = []
        for p in current_pulsars
            global repeats
            repeats = false
            for p2 in previous_pulsars
                global repeats
                if p.jname == p2.jname
                    repeats = true
                end
            end
            if repeats == false
                push!(pulsars, p)
            end
        end
        #println(length(pulsars))
        return pulsars
    end

    function new_observations(;start_date="2019-11-01")
        st = DateTime(start_date, "y-m-d")
        #push!(t_, datetime2unix(dt))
        all_pulsars = create_list(get_par=true)
        pulsars = []
        for p in all_pulsars
            global add, indexes
            add = false
            indexes = []
            for i in 1:length(p.observations)
                global add, indexes
                obs =  p.observations[i]
                de = DateTime(obs.date)
                if st < de
                    add = true
                    append!(indexes, i)
                end
            end
            if add == true
                # only new observations
                new_obs = []
                for in in indexes
                    push!(new_obs, p.observations[in])
                end
                p.observations = new_obs
                push!(pulsars, p)
            end
        end
        return pulsars
    end


    function check(jname, pulsars)
        for p in pulsars
            if p.jname == jname
                println(p)
                for obs in p.observations
                    println("\t ", trunc(Int, obs.duration/p.p))

                end
            end
        end
    end


end


#ENV["PYTHON"] = "/home/aszary/software/python-local2/bin/python"

#pulsars = MeerKAT.new_observations_old("output/pulsar_data_2019-11-05.jld")
pulsars = MeerKAT.new_observations(;start_date="2019-11-05") # 2019-11-05 done on 2019-11-16

#Tools.create_ephs()
#Tools.download_meertime_ephs()

#pulsars = MeerKAT.create_list(;get_par=true)
#println(length(pulsars))
#MeerKAT.save_list(pulsars)  # it is required to get_params  # better change it

#MeerKAT.generate_all(pulsars; verbose=1, run_script=false)
#MeerKAT.rerun_all(pulsars; force=false)
#MeerKAT.analyse_all(pulsars)
#MeerKAT.preview_all(pulsars)

#jname = "J0725-1635"
#pulse_number = 512
#MeerKAT.generate_one(jname, pulsars; pulse_number=pulse_number, run_script=true)
#MeerKAT.analyse_one(jname, nothing; pulse_num=pulse_number, obs_num=1, bin_st=375, bin_end=450)

#jname = "J1651-4246"
#pulse_number = 512
#MeerKAT.generate_onedir(jname, pulsars, 5; pulse_number=pulse_number, run_script=false)
#MeerKAT.analyse_one(jname, pulsars; pulse_num=pulse_number, obs_num=5)

#jname = "J1306-6617"
#pulse_number = 1024
#MeerKAT.generate_onedir(jname, pulsars, 1; pulse_number=pulse_number, run_script=false)
#MeerKAT.analyse_one(jname, pulsars; pulse_num=pulse_number, obs_num=1, bin_st=300, bin_end=500)

jname = "J0151-0635"
#MeerKAT.check(jname, pulsars)
pulse_number = 1036
#MeerKAT.generate_onedir(jname, pulsars, 1; pulse_number=pulse_number, run_script=false)
#MeerKAT.analyse_one(jname, pulsars; pulse_num=pulse_number, obs_num=1, bin_st=300, bin_end=500)
MeerKAT.analyse_one(jname, pulsars; pulse_num=pulse_number, number=156, obs_num=1, bin_st=300, bin_end=500)


#MeerKAT.recalculate_drifters(pulsars; filename="input/drifters2.txt")
#MeerKAT.reanalyse_drifters(pulsars; filename="input/drifters2.txt")

#MeerKAT.generate_one("J1651-4246", pulsars; pulse_number=232, run_script=false)
#MeerKAT.analyse_one("J1651-4246", nothing; pulse_num=232, obs_num=1)
#MeerKAT.analyse_one("J1651-4246", nothing)
#MeerKAT.analyse_one("J1635-4944", nothing; obs_num=5, bin_st=1, bin_end=200)

#Data.load_fits("input/pulse_6618809643.rr")

println("Bye")
